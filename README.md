# Welcome to my dotfiles

Hey! I'm Brandon Puc and this is my personal backup.        
Do you need any of this configs? Well, go to your terminal & `git clone`

# Table of contents
- [Overview](#overview)
- [Screenshots](#screenshots)
- [Installation](#installation)
- [Keybindings](#keybindings)
- [Contributing](#contributing)
- [Credits](#credits)
- [License](#license)

> **TODO:** Add my Xfce4 configs

# Overview
- Device: `Laptop`
- Operating System: `ArcoLinux`
- Window Manager: `Spectrwm`/`Bspwm`/`Qtile`
- Launcher: `Rofi`/`Dmenu` 
- Notifications: `Dunst`
- Terminal: `Alacritty`
- File mananger: `Ranger`/`Thunar`
- Editor: `neovim`
- Web Browser: `Firefox`/`Brave`
- Music Player: `Mpv`
- Wallpapers: `Nitrogen`

# Screenshots
Here are some screenshots of my finished configs. Three different flavors, same look and feel.

 ## Spectrwm
![DesktopS.png](DesktopS.png)
 ## Bspwm
![DesktopB.png](DesktopB.png)
 ## Qtile
![DesktopQ.png](DesktopQ.png)


# Installation
     
Open a terminal and...

**1. Clone my configs**
```bash
git clone https://gitlab.com/bpuc/dotfiles.git
```
**2. cd into dotfiles**
```bash
cd dotfiles
```
**3. Make the script executable**
```bash
chmod +x installer.sh
```
**4. Run the script**
```bash
./installer.sh
```
> Before installing it's important that you make a backup of your files. 

## Note:

1. Note that this is a personal script created to work on Arcolinux and may not work on other distros due to the repository packages that are installed by default.  

2. If you are using a distro other than Arco I recommend that you copy the files manually and check the `packages-repository.txt`.

3. By default, spectrwm, bspwm and qtile will be installed

## Keybindings
Here are some essential keybindings


|    Keybind    |  Application  |      Function     |
| ------------- | ------------- | ----------------- |
|`Super + Enter`| Alacritty     | Open Terminal     |
| `Super + d`   | Rofi          | Run Launcher      |
| `Super + n`   | Thunar        | Open Filemananger |
| `Super + x`   | Powermenu     | Display Powermenu |
| `Super + w`   | Firefox       | Open Browser      |
| `Alt + 1`     | Configmenu    | Open Config Files |
| `Alt + 3`     | Rofi          | Wifimenu          |
| `Alt + 5`     | Rofi          | Todo app          |
| `Alt + Shift + 5`   | Rofi    | Notes app         | 
| `Print`       | Rofi          | Take Screenshot   | 
| `Super + q`         |  wm | Close Current Window  |
| `Super + Shift + r` |  wm | Reload Configs        |


> To see all keybindings go to `spectrwm.conf`, `sxhkdrc` or `config.py`. Press "`Alt + 1`" and choose spectrwm/sxhkd/qtile respectively.

## Contributing
Comments and suggestions are welcome

## Credits
Many lines in my configurations and scripts are by inspiration or modification of original projects created by other people. Thanks to all of them for their great contribution.

- [DT](https://gitlab.com/dwt1/dotfiles)
- [Axarva](https://github.com/Axarva/dotfiles-2.0)
- [TechnicalDC](https://github.com/TechnicalDC)
- [EOS Bspwm community](https://github.com/EndeavourOS-Community-Editions/bspwm)
- [r/unixporn community](https://www.reddit.com/r/unixporn/)

## License
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)
