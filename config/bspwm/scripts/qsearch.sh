#!/usr/bin/env bash

#///////////////////////#
#       Q U I C K       #
#///////////////////////#
#      S E A R C H      #
#///////////////////////#

## Set browser
BROWSER="brave"

declare -A options

options[Google]="https://www.google.com/search?q="
options[Google Images]="googleimages - https://www.google.com/search?hl=en&tbm=isch&q="
options[DuckDuckGo]="https://duckduckgo.com/?q="
options[Amazon]="https://www.amazon.com.mx/s?k="
options[Arch AUR]="https://aur.archlinux.org/packages/?O=0&K="
options[Arch PKG]="https://archlinux.org/packages/?sort=&q="
options[Arch WIKI]="https://wiki.archlinux.org/index.php?search="
options[Deepl]="https://www.deepl.com/translator#en/es/"
options[Bing]="https://www.bing.com/search?q="
options[Reddit]="reddit - https://www.reddit.com/search/?q="
options[Stack]="stack - https://stackoverflow.com/search?q="
options[Startpage]="https://www.startpage.com/do/dsearch?query="
options[Wallhaven]="https://wallhaven.cc/search?q="
#options[GTranslate]="https://translate.google.com/?sl=auto&tl=en&text="
options[Urban Dictionary]="https://www.urbandictionary.com/define.php?term="
options[Wikipedia]="wikipedia - https://es.wikipedia.org/wiki/"
options[Etimología]="http://etimologias.dechile.net/?"
options[Youtube]="youtube - https://www.youtube.com/results?search_query="
options[Odysee]="https://odysee.com/$/search?q="
options[Alternative to]="https://alternativeto.net/software/"
options[Soundcloud]="https://soundcloud.com/search?q="
##"quit"


## Picking a search engine.
while [ -z "$engine" ]; do
enginelist=$(printf '%s\n' "${!options[@]}" | rofi -dmenu -i 10 -p "search on" -font "Iosevka Term 13" ) || exit

engineurl=$(echo "${options["${enginelist}"]}" | awk '{print $NF}')
engine=$(echo "${options["${enginelist}"]}" | awk '{print $1}')
done

## Searching the chosen engine.
while [ -z "$query" ]; do
query=$(rofi -dmenu -i 10 -p "Searching "${options["${engine}"]}"" -font "Iosevka Term 13") || exit
done

## Display search results in web browser
$BROWSER "$engineurl""$query"
