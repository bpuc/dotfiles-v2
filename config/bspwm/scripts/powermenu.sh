#!/bin/env bash

#///////////////////////#
#       P O W E R       #
#///////////////////////#
#        M E N U        #
#///////////////////////#

## Options for powermenu 
lock="  Lock"
logout="  Logout"
shutdown="  Shutdown"
reboot="  Reboot"
sleep="  Sleep"

## Get answer from user via rofi
selected_option=$(echo "$lock
$logout
$sleep
$reboot
$shutdown" | rofi -dmenu\
                  -i\
                  -p "power"\
                  -config "~/.config/rofi/powermenu.rasi"\
                  -font  "Iosevka Term 13"\
                  -width "15"\
                  -lines 5\
                  -line-margin 3\
                  -line-padding 10\
                  -scrollbar-width "0" )

## Do something based on selected option
if [ "$selected_option" == "$lock" ]
then
    ~/.config/bspwm/scripts/lock.sh 
elif [ "$selected_option" == "$logout" ]
then
    kill -9 -1 #bspc quit
elif [ "$selected_option" == "$shutdown" ]
then
    systemctl poweroff
elif [ "$selected_option" == "$reboot" ]
then
    systemctl reboot
elif [ "$selected_option" == "$sleep" ]
then
    ##amixer set Master mute
    systemctl suspend
else
    echo "No match"
fi
